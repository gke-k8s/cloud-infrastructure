## Goals
- Provision a custom virtual network within GCP (VPC)
- Provision a Kubernetes cluster using GKE
- Write an outline of AWS and GCP differences along the way

## Must watch
- [[ GCP 7 ] Creating VPC and Subnets in Google Cloud](https://www.youtube.com/watch?v=W2Wlz3PcEIQ)
- [How to Create GKE Cluster Using TERRAFORM?](https://www.youtube.com/watch?v=X_IK0GBbBTw)