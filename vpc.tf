
resource "google_compute_network" "main" {
  name                    = "main"
  auto_create_subnetworks = false

  # prohibit communications to subnets in other regions
  routing_mode = "REGIONAL"

  # we need "0.0.0.0/0 -> The Internet" route
  delete_default_routes_on_create = false

  depends_on = [
    google_project_service.compute,
    google_project_service.container
  ]
}

resource "google_compute_subnetwork" "main_public" {
  name          = "public"
  ip_cidr_range = "10.0.0.0/24"
  region        = "europe-north1"
  network       = google_compute_network.main.id

  private_ip_google_access = false
}

resource "google_compute_subnetwork" "main_private" {
  name          = "private"
  ip_cidr_range = "10.0.1.0/24"
  region        = "europe-north1"
  network       = google_compute_network.main.id
  # allow access google services without external ip
  private_ip_google_access = true
}

resource "google_compute_router" "main" {
  name = "main"

  network = google_compute_network.main.id
  region  = "europe-north1"

  bgp {
    asn = 64514
  }
}


resource "google_compute_router_nat" "nat" {
  name   = "main"
  router = google_compute_router.main.name
  region = google_compute_router.main.region

  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = google_compute_subnetwork.main_private.name
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

# in case you need ssh
resource "google_compute_firewall" "allow-ssh" {
  name    = "allow-ssh"
  network = google_compute_network.main.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}
