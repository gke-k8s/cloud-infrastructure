# setup credentials with
# export GOOGLE_APPLICATION_CREDENTIALS=$(readlink -f ~/.gcp/service-account.json)
# if you use service account

# for non-service acount you simply login

# there is also roles/config.agent specifically for
# tools like terraform
terraform {
  backend "gcs" {
    bucket = "serost-k8s-pet-tfstate"
    prefix = "/remote-state"
  }
}

provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
  zone    = var.gcp_zone
}
