# Don't forget to add "Quota Administrator" role
# and enable Service API thing (you'll have errors otherwise)
# StackOverflow thread: https://stackoverflow.com/questions/70900347/terraform-gcp-error-403-permission-denied-to-list-services-for-consumer-cont

# enable api services
resource "google_project_service" "compute" {
  service = "compute.googleapis.com"
}

resource "google_project_service" "container" {
  service = "container.googleapis.com"
}
